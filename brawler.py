import bs4
import requests
from bs4 import BeautifulSoup, Tag


class Brawler:
    def __init__(self):
        pass

    @staticmethod
    # retrieve html content by http GET :param(url) as string
    def __fetch_html_text(url):
        return requests.get(url).text

    @staticmethod
    # create BeautifulSoup instance as 'html.parser' for string content :param html
    # Note: Beautiful Soup is a Python library for pulling data out of HTML and XML files.
    #   (https://beautiful-soup-4.readthedocs.io/en/latest/)
    def __soup_parser(html):
        return BeautifulSoup(html, 'html.parser')

    def find_child_element_by_html_tag_filtered_by_attribute(self, element: Tag, tag: str, filters: dict = None):
        elements = element.find_all(tag)
        return self.__apply_filters(elements, filters)

    def elements_by_html_tag_filtered_by_attribute(self, url: str, tag: str, filters: dict = None):
        """
        Find all web elements within the html content of the GET :param (url) requests response body text.

        Elements are identified by the provided html tag (e.g. <div>, <p>, <a>) and optionally
        filtered by provided attribute key value pairs.

        If no filter value is supplied or is None no filter is applied.

        Found elements are filtered by each provided key-value pair of :param (filters).
        The result contains only elements where an attribute for the key is present and the value for
        that attribute contains (case-insensitive) the value of the filter pair.

        In case of multiple key-value pairs the filters are combined with a logical AND, meaning an
        element has to meet all filters in order to NOT be excluded from the result.

        example method calls:
        - get all <div> elements that have 'title' in their 'class' attribute from 'https://whaatever.wiki.ui/ba/Tree'
            find_web_elements_by_html_tag_filtered_by_attribute(
                url = 'https://whaatever.wiki.ui/ba/Tree',
                tag: 'div',
                filters = {'class' : 'title'}
            )
        - get all <a> elements that contain '/ba/Tree' in their 'href' attribute as well as value 'schwert' in 'title'.
            find_web_elements_by_html_tag_filtered_by_attribute(
                url = 'https://whaatever.wiki.ui/ba/Tree',
                tag: 'a',
                filters = {'href': '/ba/Tree', 'title': 'schwert'}
            )

        Parameters:
        :param url: The url of the web page to fetch the html content from. Provided as Fully
            qualified URL (e.g. 'https://exampliii.com/some/path/to').
        :param tag: Html tag identifier as string (e.g. 'div','a' or 'p').
        :param filters: Dictionary to filter found elements by contained attribute values identified by
            their key. Elements have to match all filters. If None if supplied no filters are applied
            (e.g. {'class': 'title', 'href': '/de/wiki'}).

        Returns:
        list[Tag]: List of all found web elements optionally filtered by their attributes.
        """
        soup = self.__soup_parser(self.__fetch_html_text(url))
        elements = soup.find_all(tag)

        result = self.__apply_filters(elements, filters)
        return result

    def __apply_filters(self, elements: list[Tag], filters: dict):
        result = []
        for element in elements:
            add = True
            if filters:
                for key, value in filters.items():
                    if key in element.attrs:
                        if value not in element[key]:
                            add = False
                    else:
                        add = False
            if add:
                result.append(element)
        return result

    def parse_to_list_of_dicts(self, elements: list[Tag]):
        result = []
        for elem in elements:
            result.append(self.__extract_info(elem))
        return result

    def __extract_info(self, element: Tag):
        """
        Transform a provided :param (element) to a dict.

        The resulting dictionary consists of all attributes of the given element
        with the attributes key and values as key-value pairs of the result dict.

        For all children of element this method is called recursively and the resulting
        dict of the recursive call is added as entry to the result with the child 'name' as key
        and a dict of the child attributes (or children) as value.

        Parameters:
        :param element: The Beautiful Soup object to be transformed into a dict

        Returns:
        dict: Dict containing all primitive attributes of the element as string key-value pairs and
            all child elements as key-value pairs with the child name as key and the child attributes transformed
            to a dict as the dict entries value.
        """
        result = {}

        # add keys with primitives to result
        attrs = element.attrs
        result.update(attrs)

        # get all children of the element
        children = element.find_all()

        # in case children exist call this method recursively for each one
        # the resulting dict is added to this result with the child name as key
        if children:
            for child in children:
                result[child.name] = self.__extract_info(child)

        # return dict with all elements attributes and children as dict attribute
        return result

    def elements_by_tag_filtered_by_attributes(self, url, tag, key, value):
        result = []
        for elem in self.elements_by_tag(url, tag):
            if self.__attr_contains(elem, key, value):
                result.append(elem)
        return result

    def __apply_optional_filter(self, elements, attr_key, attr_value):
        for element in elements:
            if attr_value in element.get(attr_key):
                yield element

    @staticmethod
    def __attr_contains(element, key, value):
        attr = element.get(key)
        if attr is None:
            return
        return value in element.get(key)

    def elements_by_tag(self, url, tag):
        result = []
        html = self.__fetch_html_text(url)
        soup = self.__soup_parser(html)
        for element in soup.find_all(tag):
            result.append(element)
        return result

    @staticmethod
    def all_children_by_tag(parent, tag):
        yield parent.find_all(tag)

    @staticmethod
    def children_by_tag(parent, tag):
        result = []
        for child in parent.find_all(tag):
            result.append(child)
        return result

    @staticmethod
    def elem_attr_for_keys(elem, keys=[]):
        result = {}

        # return an empty dictionary if the keys are empty
        if len(elem) == 0:
            return result

        if isinstance(elem, Tag):
            attr = elem.attrs
            for key in keys:
                if key in attr:
                    result[key] = attr[key]
        elif isinstance(elem, list):
            for e in elem:
                if isinstance(e, Tag):
                    attr = e.attrs
                    for key in keys:
                        if key in attr:
                            result[key] = attr[key]

        return result

    def get_all_filtered_elements_attributes(self, url, tag, keys_with_replace):
        elems = self.elements_by_tag(url, tag)
        result = []
        for elem in elems:
            result.append(self.elem_attr_for_keys(elem, keys_with_replace))
        return result
