import random
from typing import Any


# generate a list of :size with random dicts matching in keys and data types
def generate_list_of_random_dicts(size: int, key_types: dict):
    result = []
    for i in range(size):
        result.append(generate_random_dict(key_types))
    return result


# generate a random dict using provided :key_types for key names and data types
def generate_random_dict(key_types: dict):
    result = {}
    for key, data_type in key_types.items():
        result[key] = generate_random_variable(data_type)
    return result


# generate a random variable of the specified :data_type
def generate_random_variable(data_type, size=10, range_start: int = 0, range_end: int = 100) -> Any:
    if data_type == str:
        return ''.join(random.choices('abcdefghijklmnopqrstuvwxyz', k=size))
    elif data_type == int:
        return random.randint(range_start, range_end)
    elif data_type == float:
        return random.uniform(range_start, range_end)
    elif data_type == bool:
        return random.choice([True, False])
    else:
        raise ValueError(f"Unsupported data type: {data_type}")
