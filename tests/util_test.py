import unittest
import pathlib as pl

from tests.test_util import generate_random_variable, generate_random_dict, generate_list_of_random_dicts
from util import flatten_dict_recursively, save_list_of_dict_as_csv, validate_dict_matching_keys


class TestBase(unittest.TestCase):
    def assertIsFile(self, path):
        if not pl.Path(path).resolve().is_file():
            raise AssertionError("File does not exist: %s" % str(path))


class FlattenTest(TestBase):
    def test_flatten_dict_recursively(self):
        # Arrange
        input_dict = {
            'a': 'foo',
            'b': {
                'X': 'bar',
                'Y': 'baz',
                'Z': {
                    'AA': 'foo'
                }
            },
            'c': 'bar',
            'd': 'baz'
        }
        expected = {
            'a': 'foo',
            'b-X': 'bar',
            'b-Y': 'baz',
            'b-Z-AA': 'foo',
            'c': 'bar',
            'd': 'baz'
        }
        # Act + Assert
        self.assertDictEqual(flatten_dict_recursively(input_dict), expected)


class SaveCsvTest(TestBase):
    def test_list_of_dict_to_csv(self):
        # Arrange
        keys = ['name', 'desc', 'age', 'value']
        input_data = generate_list_of_random_dicts(
            2,
            {keys[0]: str, keys[1]: str, keys[2]: int, keys[3]: float}
        )
        expected_file_full_path = 'output/test-out/csv/test-csv.csv'

        # Act
        actual: list[list[str]] = save_list_of_dict_as_csv(input_data, keys, 'test-csv', ['test-out', 'csv'], ';')

        # Assert saved file by path and filename
        self.assertIsFile(expected_file_full_path)

        # Assert equal size of input and actual
        self.assertEqual(len(input_data) + 1, len(actual))

        header: str = actual[0].pop()
        data: list[str] = [actual[1].pop(), actual[2].pop()]

        # Assert only if all keys are part of the header
        for key in keys:
            self.assertTrue(key in header)

        # Assert only if all values of input data are in the csv
        for input_single in input_data:
            for value in input_single.values():
                self.assertTrue(str(value) in ' '.join(data))


if __name__ == '__main__':
    unittest.main()
