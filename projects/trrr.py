from brawler import Brawler
from util import remove_duplicates_by_key, extract_keys, download_images, validate_dict_matching_keys, \
    validate_dict_keys_and_repair, flatten_dict_recursively, save_list_of_dict_as_csv

__LANGUAGE_CODE__ = 'de'
__BASE_URL__ = f'https://terraria.wiki.gg/{__LANGUAGE_CODE__}/wiki'
__WEAPON_URL_PATH__ = '/Waffen'


def test_stuff_yes_good_name():
    print('Find weapon meta infos. url: https://terraria.wiki.gg/de/wiki/Waffen')
    all_weapon_meta = __find_all_weapons_meta_info()
    print(f'Found weapon meta count: {len(all_weapon_meta)}.')

    meta_match = validate_dict_matching_keys(all_weapon_meta)
    print(f'All found meta infos have identical keys: {meta_match}.')
    # TODO breaks for now if not matching but do match

    all_weapon_stats: list[dict] = []
    print('Find stats for each meta href value, e.g. https://terraria.wiki.gg/de/wiki/Rally for href=/de/wiki/Rally.')
    print(f'Take stats info only if fetched value not None.')
    print(f'Also add title retrieved from meta[\'title\'] as key \'weapon-title\' to each found weapon.')

    for meta in all_weapon_meta:
        stats = get_weapon_stats(__get_full_weapon_url(meta['href']))

        if type(stats) is dict:
            stats['weapon-title'] = meta['title']
            all_weapon_stats.append(stats)

    print(f'Retrieve non-Null stats count: {len(all_weapon_stats)}.')

    stats_match = validate_dict_matching_keys(all_weapon_stats)
    print(f'Stats dicts all have matching keys: {stats_match}.')

    wanted_stat_keys = ['weapon-title', 'Typ', 'VerwendetMunition', 'Schaden', 'Rückstoß', 'Mana', 'KT-Chance',
                        'Ben.geschw.', 'Tooltip', 'Proj.geschw.', 'Seltenheit', 'Erwerben', 'Verkaufen', 'Erforschen',
                        'Verbrauchbar', 'Platzierbar', 'VerwendetMunition', 'Max. Stapel']

    print(f'TODO: Keys used for now. Maybe not every key ever used in any weapon!!! For reasons the save csv should'
          f'(and did, alot :D) throw an error on occurence.')
    print(f'Stat_keys: {wanted_stat_keys}')
    print('Filling all missing keys for each weapon so we have a unified scheme.')

    unified_stats = validate_dict_keys_and_repair(all_weapon_stats, keys=wanted_stat_keys)
    unified_stats_match = validate_dict_matching_keys(unified_stats)

    print(f'Unified stats dicts all have matching keys: {unified_stats_match}.')

    print(f'Extracting image urls from meta infos for later download.')
    image_download = {}
    for meta in all_weapon_meta:
        title = meta['title']
        url = meta['img']['src']
        if title and url:
            image_download[title] = url
    print(f'Extracted number of image urls: {len(image_download)}')

    print(f'Saving images to /output/images/weapons/<weapon-name>')
    image_folders = ['images', 'weapons']
    download_images(image_download, image_folders)

    print(f'Flatten meta and stat dicts.')
    flattened_meta = []
    for meta in all_weapon_meta:
        flattened_meta.append(flatten_dict_recursively(meta))

    flattened_stats = []
    for stats in all_weapon_stats:
        flattened_stats.append(flatten_dict_recursively(stats))

    print(f'Flattened meta count: {len(flattened_meta)}')
    print(f'Flattened stats count: {len(flattened_stats)}')

    print(f'Saving meta to /output/csv/weapon-meta.csv')
    wanted_meta_keys = ['href', 'title', 'img-src', 'img-width', 'img-height']
    save_list_of_dict_as_csv(flattened_meta, wanted_meta_keys, 'weapon-meta', ['csv'], ';', False)

    print(f'Saving stats to /output/csv/weapon-stats.csv')
    save_list_of_dict_as_csv(flattened_stats, wanted_stat_keys, 'weapon-stats', ['csv'], ';', False)


def __find_all_weapons_meta_info() -> list:
    brawler = Brawler()

    url = f'{__BASE_URL__}{__WEAPON_URL_PATH__}'
    tag = 'div'
    filters = {'class': 'itemlist'}
    item_tables = brawler.elements_by_html_tag_filtered_by_attribute(url, tag, filters)

    result = []
    for table in item_tables:
        table_items = brawler.find_child_element_by_html_tag_filtered_by_attribute(table, 'a', None)
        item_dicts = brawler.parse_to_list_of_dicts(table_items)
        unique_items = remove_duplicates_by_key(item_dicts, 'title')
        filtered_unique_items = extract_keys(unique_items, ['href', 'title', 'src', 'height', 'width'])
        for key, value in filtered_unique_items.items():
            result.insert(len(result), value)

    return result


def get_weapon_stats(url: str) -> dict or None:
    brawler = Brawler()

    stat_tables = brawler.elements_by_tag_filtered_by_attributes(url, 'table', 'class', 'stat')
    stat_table = None
    if len(stat_tables) >= 1:
        stat_table = stat_tables[0]
    else:
        return None

    rows = brawler.find_child_element_by_html_tag_filtered_by_attribute(stat_table, 'tr')

    result = {}

    for row in rows:
        title = row.find('th').text
        value = row.find('td').text.replace(u'\xa0', u' ')
        if title is not None:
            result[title] = value

    return result


def __get_full_weapon_url(weapon_url: str):
    name_segment = weapon_url.split('/')[-1]
    return f'{__BASE_URL__}/{name_segment}'
