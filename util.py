import csv
import os
import random
from enum import Enum

import requests


class RepairType(Enum):
    FILL_OR_REDUCE = 1
    REMOVE_DICT = 2


def validate_dict_keys_and_repair(
        dicts: list[dict],
        keys: list[str] = None,
        repair_type: RepairType = RepairType.FILL_OR_REDUCE
) -> list[dict]:
    if keys is None:
        keys = list(dicts[0].keys())
    result = []
    for d in dicts:
        if d is None:
            print(f'Dict is None {d}')
            continue
        if set(keys) == set(list(d.keys())):
            result.append(d)
        else:
            if repair_type == RepairType.REMOVE_DICT:
                continue
            elif repair_type == RepairType.FILL_OR_REDUCE:
                result.append(__fill_or_reduce_by_keys(d, keys))
    return result


def extract_keys(collection: dict, wanted_keys: list):
    keys_to_delete = []
    for key, value in collection.items():
        if isinstance(value, dict):
            collection[key] = extract_keys(value, wanted_keys)
        elif isinstance(value, str):
            if key not in wanted_keys:
                keys_to_delete.append(key)
    for key in keys_to_delete:
        del collection[key]
    return collection


def remove_duplicates_by_key(collection: list[dict], key: str):
    uniques = {}
    for item in collection:
        if key not in item.keys():
            continue
        else:
            try:
                if uniques[item[key]]:
                    if len(item) > len(uniques[item[key]]):
                        uniques[item[key]] = item
            except KeyError:
                uniques[item[key]] = item

    return uniques


def flatten_dict_recursively(dictionary: dict, prefix: str = None) -> dict:
    result = {}
    for key, value in dictionary.items():
        if isinstance(value, dict):
            result.update(flatten_dict_recursively(value, f'{prefix}-{key}' if prefix else key))
        else:
            if prefix:
                result[f'{prefix}-{key}'] = value
            else:
                result[key] = value
    return result


def validate_dict_matching_keys(dicts: list[dict], keys: dict.keys = None) -> bool:
    if keys is None:
        keys = dicts[0].keys()
    for d in dicts:
        if len(keys) != len(d.keys()):
            return False
        if set(keys) != set(d.keys()):
            return False
    return True


def save_list_of_dict_as_csv(
        list_of_dicts,
        field_names: list[str],
        filename=None,
        folders: list[str] = None,
        delimiter=';',
        return_result: bool = True
) -> list[list[str]] or None:
    if list_of_dicts is None:
        raise ValueError('Supplied dicts must not be None')

    if list_of_dicts[0] is None or len(list_of_dicts[0].keys()) < 1:
        raise ValueError('A minimum of 1 dict with have at least one key must be supplied.')

    # filename = __normalize_filename(filename, 'csv') # TODO make it new since logic name blabla

    # Extract fieldnames from the first dictionary in the list
    # fieldnames = list_of_dicts[0].keys()

    full_file_path = __get_full_file_path(folders, filename, 'csv')

    # Write data to CSV file
    with open(full_file_path, 'w', newline='') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=field_names, delimiter=delimiter)
        writer.writeheader()  # Write header with fieldnames
        for row in list_of_dicts:
            try:
                writer.writerow(row)
            except ValueError as e:
                print(f'Error writing row as csv: {row}, error: {e}')
                continue

    if return_result:
        with open(full_file_path, 'r') as file:
            csv_reader = csv.reader(file)
            return list(csv_reader)


def download_images(image_names_and_urls: dict[str, str], folders: list[str] = None) -> list:
    saved = []
    for name, url in image_names_and_urls.items():
        img_path = download_image(url, name, folders)
        if img_path:
            saved.append(img_path)
    return saved


def download_image(url: str, name: str, folders: list[str] = None) -> str or None:
    """
    Save the content of a http GET request for :param (url) as image file
    with the provided :param (name) and file extension extracted from the url.
    If the extracted extension is not valid and therefor None the image download is
    cancelled.

    Parameters:
    url (str): The fully qualified URL of the image.
    name (str): The name used to for the file to save the image data to.

    Returns:
    bool: True only if the image was successfully downloaded and saved, otherwise False.
    """
    ext = __extract_image_file_extension_from_url(url)
    if ext is None:
        print(f'Error getting file extension from url: {url}')
        return None

    response = requests.get(url)

    if response.status_code == 200:
        try:
            full_file_path = __get_full_file_path(folders, name, ext)
            with open(full_file_path, 'wb') as file:
                file.write(response.content)
            return full_file_path
        except (IOError, OSError, PermissionError, TypeError) as e:
            print(f'Error writing image to file: {e}')
    else:
        print(f"Error downloading image for {url}. {response.status_code}: {response.text}")
    return None


def __extract_image_file_extension_from_url(url: str):
    """
    Check if the provided :param (url) ends with '.<valid_extension>'.
    All extensions matching any of :valid_extension: are considered valid.

    Parameters:
    url (str): The URL to extract the file extension from.

    Returns:
    str or None: The extracted file extension if it's valid, otherwise None.
    """
    valid_extensions = ['png', 'jpg', 'jpeg', 'gif']
    last_dot_index = url.rfind('.')
    image_format = url[last_dot_index + 1:]
    if image_format in valid_extensions:
        return image_format
    else:
        return None


def __random_str(length: int = 5) -> str:
    return ''.join(random.choices('abcdefghijklmnopqrstuvwxyz', k=length))


def __get_full_file_path(folders: list[str], name: str, ext: str, out_dir: str = 'output') -> str:
    full_dir_path = os.path.join(__get_project_root(), out_dir)

    for folder in folders:
        full_dir_path = os.path.join(full_dir_path, folder)
        os.makedirs(full_dir_path, exist_ok=True)

    file_name = f'{name}.{ext}'
    return os.path.join(full_dir_path, file_name)


def __get_project_root() -> str:
    """Returns the root directory of the current Python project."""
    # Start from the current directory
    current_dir = os.getcwd()

    # Navigate upwards until a marker file is found
    while not any(filename in os.listdir(current_dir) for filename in ('__init__.py', 'setup.py', 'requirements.txt')):
        # Move up one directory
        current_dir = os.path.dirname(current_dir)

        # Check if we've reached the root directory
        if current_dir == os.path.dirname(current_dir):
            raise RuntimeError("Project root not found. Make sure you have a marker file in your project directory.")

    return current_dir


def __fill_or_reduce_by_keys(input_data: dict, keys: list[str]) -> dict:
    input_keys = list(input_data.keys())
    result = {}

    for key in keys:
        if key in input_keys:
            result[key] = input_data[key]
        else:
            result[key] = ''

    return result
