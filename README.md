# Betti Labs
Python based web crawler using basic principles to be used for gathering use case specific
information from a web pages html content.

### Note
The following game specific documentation is a living document and may be altered as needed.
In case of braking or any other mayor changes please consult with another involved person.

## Architecture
Basic Architectural ideas and schemes as result of the first planning iteration on the project's logic. 


### Goal
A web brawler with the ability to fetch any kind of web element Sof a provided html content with use of standard html selectors.
This also includes executing these functions on e.g. children of web elements (children refers to web elements that
are located within another called 'parent' element within the html).

The gathered information can be optionally reformatted/filtered to get the desired result. The way module handles these
actions should also work in a generic way which is than used for this project's specific use case but also
for future project's with a minimum of effort.

#### Low Prio Goals
- module to download images with whatever format/metadata and handle it in the desired way
- think of a way to handle project specific stuff for
  UX friendly without the need to study the html for every use case.


### Modules
Modules used in the project. Needed modules, their naming and possible changes to capabilities or their intended may be altered as needed.
More that a short description about the purpose of each can be found in the (wip) TODO list under [TODO steps](./docs/todo_steps.md).

#### Crawler Module
Basic web crawler fetching html content by using HTTP requests.

**Purpose**:
Provide fetched raw data from html content.

#### Content Processor Module
Basic data processing to enhance the information.

**Purpose**:
Use received raw data as a base for optionally actions like structuring, cleaning or filtering
to provide the desired result.

#### <Name i may shall not write here> specific analysis
Functionalities with respect to this project's original use case

**Purpose:**
Gather information needed as final result from already cleaned and structured data.


## Python
Basic information and commands for the python language and related subjects.

#### Installation
Follow the OS specific instructions of the official website [Python](https://www.python.org/downloads/).

### Venv
A project specific virtual environment. All project dependencies are installed within this environment as mentioned in [Package Managing](#package-managing).

In order to work inside a venv it has to be activated. While a venv is activated all pip installations
are within the venv.

A venv can also be deactivated in order to work again independently of the venv.

Create a new venv if none is existing in the project yet e.g. in a folder called '.venv'.
```bash
python -m venv .\.venv
```

Install pip in the venv (after it's activated, per default after creation)
```bash
py -m ensurepip --upgrade
```

Update pip
```bash
pip install --upgrade pip
```

To activate a venv run the activation script from the project's venv files (Windows only).
```bash
.\.venv\bin\activate
```

To deactivate a venv use the deactivate command from the project's root directory (Windows only).
```bash
deactivate
```

To check if a venv is currently active the following pip command can be used. If so the command shows the pip executable of the venv which is located in the project's venv files (e.g. \.venv\lib\site-packages\pip).
```bash
pip -V
```
In a terminal the following command can be used if the venv is active.

For more information check out the official documentation [Python Venv](https://docs.python.org/3/library/venv.html).

### Package Managing
The *requirements.txt* file serves as a centralized location to specify all the dependencies required for the project.
It ensures that all necessary libraries are installed to run the project.

#### Add packages
Libraries can be installed using pip. Ensure that the Venv is currently active and run pip install for the desired
```bash
pip install <package-name>
```

#### Save requirements
Save all currently in the venv installed libraries as *requirements.txt* file.

```bash
pip freeze > requirements.txt
```

#### Install requirements
```bash
pip install -r requirements.txt
```