# TODO
List of modules to be implemented or any other actions to take.
On architectural or any other actions necessary the list has to be updated as needed.
Ideally in a structured way.
Feel free to add questions/subtasks whatever.

## Modules
- [ ] Brawler 
  - [X] Some Basic functionality
- [ ] Generic Content Processor
  - [X] Some util methods

## Tests
- [ ] Brawler
- [ ] Util
  - [X] Flatten dict
  - [ ] ...

## Projects
- [ ] Trrr
  - [X] Basic use-case for weapons